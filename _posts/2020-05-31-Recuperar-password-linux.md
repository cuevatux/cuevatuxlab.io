---
layout: post
title: "Recuperacion de password"
date: 2020-05-31 21:52
categories: Linux,SoftwareLibre
---

# Recuperacion de password en Linux

    > 8 8 8 8                     ,ooo
    > 8a8 8a8                    oP   ?b
    >d888a888zzzzzzzzzzzzzzzzzzzz8     8b
    > ""^""'                     ?o___oP
<p>

Hola de nuevo, aqui vengo de nuevo a compartir con ustedes una anecdota que me ocurrio hace unas semanas atras. Me disponia a empezar a trabajar como de costumbre, cuando de repente paso lo impensable , se me habian olvidado por completo mis password de mi laptop, algo que sinceramente no concebia que hubiera pasado.

</p>
<p>
Bueno, pues no me quedo de otra mas que recordar como recuperar tales passwords de mi laptop que tengo con Fedora y pues es aqui donde les voy a compartir este metodo para que puedan recuperar sus passwords en linux y no pasen este mismo acto amargo que me sucedio.
</p>

<p>
Empecemos por encender nuestra computadora y una vez que nos muestre la pantalla de seleccion de kernel, posicionamos la seleccion en el ultimo kernel que tenemos, pero no presiones enter o la flecha derecha , por que si no iniciaremos el proceso de boot de nuestro linux. Una vez posicionados en el ultimo kernel ahora si presionamos la tecla ' e ' para poder editar la linea del kernel.

</p>


<img src="https://cuevatux.gitlab.io/assets/images/password-recovery/pantalla-seleccion.jpg" class="align-center" alt="pantalla-seleccion" width="400"/>



<p>

Buscaremos la linea de kernel que empieza con **linux**, posicionamos el cursor hasta el final de esta linea y agregaremos lo siguiente:

</p>


>S init=/bin/bash


<img src="https://cuevatux.gitlab.io/assets/images/password-recovery/linea-linux1.jpg" class="alig-center" alt="linea" width="400"/>

<p>

Despues de agregar esta linea al final pulsaremos crtl+x para iniciar el proceso de boot. En algunas otras distribuciones pide pulsar la tecla 'b'.

</p>

<p>

Una vez que el boot haya terminado, nos dara un prompt como **root**, estando ahi ya podremos ver las particiones de nuestro linux instalado. Ahora aqui lo que no interesa es recuperar nuestros password, podemos recuperar el password de nuestro usuario o el de root, cualquiera.

</p>

<img src="https://cuevatux.gitlab.io/assets/images/password-recovery/root-prompt.jpg" class="alig-center" alt="root-prompt" width="400"/>

<p>

Estando en el prompt, lo primero que debemos de hacer es poner el FS de root ("/") en modo de escritura/lectura, ya que este queda montado en modo de solo lectura. Es sencillo remontar la particion con este comando:

</p>

>$ mount -o remount,rw /

<p>

Si no regresa ningun error habremos remontado con exito el fs de root y ahora podremos escribir sin problemas.

</p>
<p>

Ahora procederemos a lo siguiente, en nuestro prompt teclearemos el comando siguiente:

</p>

>$ passwd usuario_al_que_modificaremos_password

<p>

Nos pedira un nuevo password para el usuario al cual le restauraremos su password. El password lo pedira dos veces para comprobarlo.

</p>

<p>

Nos regresara un mensaje de que el password a sido actualizado. Hecho todo esto, ya podremos reiniciar nuestro equipo e ingresar con el nuevo
password.

</p>

<p>

Espero que esto les sea de gran ayuda como a mi me lo fue, por que olvidar el password de tu laptop de trabajo no es nada agradable.

</p>

Si surge alguna duda o comentario no duden en mandar un [twitt](https://twitter.com/cueva_tux) y con gusto les ayudo.
