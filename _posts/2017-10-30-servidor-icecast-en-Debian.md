---
layout: post
title: "Servidor Icecast en Debian"
date: 2017-10-30 1:02
categories: Linux,SoftwareLibre
---

<img src="https://cuevatux.gitlab.io/assets/images/logo_de_icecast.jpg" class= "align-center" alt= "logo_de_icecast">

Asi que en este post te compartire los pasos para la instalacion y configuracion del servidor en una distro Debian.


Lo primero que haremos sera abrir una terminal y escribir el siguiente comando:

$ sudo apt-get install icecast2

Una vez que termine de descargar e instalar el servidor , seguiremos por instalar las dependencias de icecast:

$ sudo apt-get install libogg-dev vorbis-tools liblamemp3 libxml2 libxslt libmp3lame-dev

De igual manera esperamos a que termina la instalacion.

Ya con todo esto instalado, procedemos a ir a la siguiente ruta :

$ cd /etc/icecast2

Dentro del directorio encontraremos el archivo de configuracion icecast.xml, en cual modificaremos algunos parametros. Mostrare la configuracion basica , para poder empezar a usarlo de manera basica:

```
<limits>
<clients>100</clients>
<sources>2</sources>
<queue-size>524288</queue-size>
<client-timeout>30</client-timeout>
<header-timeout>15</header-timeout>
<source-timeout>10</source-timeout>
<!– If enabled, this will provide a burst of data when a client
first connects, thereby significantly reducing the startup
time for listeners that do substantial buffering. However,
it also significantly increases latency between the source
client and listening client. For low-latency setups, you
might want to disable this. –>
<burst-on-connect>1</burst-on-connect>
<!– same as burst-on-connect, but this allows for being more
specific on how much to burst. Most people won’t need to
change from the default 64k. Applies to all mountpoints –>
<burst-size>64000</burst-size>
</limits>
```

Bien la etiqueta de "clients", aqui difinimos la cantidad de usuarios que realizaran conexion con nuestro servidor, esto dependera mucho de tu conexion a internet.


```
<authentication>
<!– Sources log in with username ‘source’ –>
<source-password>HACkME</source-password>
<!– Relays log in with username ‘relay’ –>
<relay-password>HACKME</relay-password>

<!– Admin logs in with the username given below –>
<admin-user>admin</admin-user>
<admin-password>HACKME</admin-password>
</authentication>
```

En esta seccion del archivo , modificaremos 3 lineas que son la de ‘ source-password’ , ‘relay-password’ y ‘admin-password’ . En estos campos pondremos las contraseñas que nos permiten el acceso al servidor , respectivamente , source seria la fuente del audio que entraria al servidor, relay se ocupa para cuando tenemos varios servidores interconectados entre si y por ultimo admin, es el usuario que nos dejara ver el estatus , puntos de montajes ,etc en el servidor. Como nota el usuario admin puede ser cambiado en la etiqueta ‘ admin-user’.
```
<hostname>localhost</hostname>

<!– You may have multiple <listener> elements –>
<listen-socket>
<port>8001</port>
<!– <bind-address>127.0.0.1</bind-address> –>
<!– <shoutcast-mount>/stream</shoutcast-mount> –>
</listen-socket>
```

En esta seccion , podremos cambiar el nombre del servidor , en la etiqueta ‘hostname’.

Lo siguiente en el arcuhivo en la configuracion de como y por donde se comunicara nuestro servidor. La etiqueta ‘ port’ es donde asignaremos el puerto de escucha del servdidor. El ‘ bind-address’ es la direccion ip de nuestro servidor, con la cual nos conectaremos. El ‘ shoutcast-mount’ sera el punto de montaje de nuestras transmisiones , ya se stream en vivo o listas de reproduccion.



Basicamente con esta configuracion ya puede empezar a trabajar nuestro servidor Icecast ; les meciono algunas aplicaciones para realizar stream en vivo o transmision de musica ( son las que yo uso ).

1. [Mixxx](https://www.mixxx.org/)

2. [Butt](https://sourceforge.net/projects/butt/)

Espero que este pequeño tutorial les sea de mucha utilidad , no duden en contactarme si les surge alguna duda.
