---
layout: post
title:  "Cuevatux piloto podcast"
date:   2017-10-26 18:17
categories: GNULinux,Podcast
---
El titulo lo dice todo , por fin tengo grabado un primer episodio , que a continuacion les dejare :


<audio controls="">
  <source src="https://cuevatux.gitlab.io/Audios/Piloto_CuevaTux.mp3" type="audio/mpeg" />
</audio>

En este primer episodio , hablo acerca de como entre al mundo de Linux y por que las personas se detienen en usar software libre , en concreto linux.



Espero que sea de su agrado , siendo mi primera vez que grabo un podcast , estoy abierto a recibir opiniones. Nos vemos en un proximo episodio ! .
