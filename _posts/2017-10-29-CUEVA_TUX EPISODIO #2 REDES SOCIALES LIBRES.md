---
layout: post
title:  "Cuevatux episodio #2 Redes Sociales Libres"
date:   2017-10-29 23:17
categories: GNULinux,Podcast
---

Listo el episodio #2 de CuevaTux , hablemos de redes sociales libre , he usado dos plataformas libres muy buenas y que me han llenado el ojo , son Mastodon que es equivalente a Twitter y Diaspora que es equivalente a Facebook.

Las dos redes son de codigo abierto , libres de publicidad. Les dejo el audio para que lo escuchen y compartan su opinion.

<audio controls="">
  <source src="https://cuevatux.gitlab.io/Audios/Cueva_Tux_2_Redes_Sociales_Libres.mp3" type="audio/mpeg" />
</audio>
