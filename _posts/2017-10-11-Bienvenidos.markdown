---
layout: post
title:  "Bienvenidos"
date:   2017-10-11 19:17
categories: Noticias
---

Bienvenidos

En estos dias he estado probando el uso de jekyll , un sistema para blog de forma
estatica, el cual la verdad me parece bastante agradable.

Lo seguire probando, aunque ahora tendre que aprender un poco de markdown, para hacer
los articulos.

Hasta la proxima!


[Twitter](https://twitter.com/cueva_tux)

[Facebook](https://www.facebook.com/cuevatux/)

[Mastodon](https://mastodon.social/web/accounts/196022)


[iVoxx](http://mx.ivoox.com/es/podcast-cuevatux_sq_f1448015_1.html)

[Spreaker](https://www.spreaker.com/show/2662792)

[YouTube](https://www.youtube.com/channel/UC3IHegvWAtxNU7IG5vqFRDw)
